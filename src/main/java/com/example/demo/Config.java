package com.example.demo;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:jdbc.properties")
public class Config {

    @Value("${jdbc.driverClassName}")
    private String driverClassName;
    @Value("${jdbc.url}")
    private String jdbcURL;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;
    private static final Logger logger = LoggerFactory.getLogger(Config.class);

  @PostConstruct
  private void printdata() {

    logger.info("driverClassName: {}", driverClassName);
    logger.info("Url: {}", jdbcURL);
    logger.info("User name: {}", username);
    logger.info("Password: {}", password);
  }

    @Bean
    public DataSource getDataSource() {
        final BasicDataSource dataSource= new BasicDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(jdbcURL);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }
}
