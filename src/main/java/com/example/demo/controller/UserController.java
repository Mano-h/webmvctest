package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/user")
public class UserController {

  private UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> findUserById(@PathVariable("id") Long id) {
    return new ResponseEntity(userService.findUserById(id), HttpStatus.OK);
  }

  @PostMapping("/")
  public ResponseEntity<User> saveUser(@RequestBody User user) {
    return new ResponseEntity(userService.save(user), HttpStatus.CREATED);
  }
}
