package com.example.demo.service;

import com.example.demo.entity.User;


public interface UserService {
  public User findUserById(Long id);

  public User save(User user);
}
