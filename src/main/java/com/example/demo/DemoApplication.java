package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.*;



@SpringBootApplication
@RestController
@PropertySource("classpath:jdbc.properties")
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }
}