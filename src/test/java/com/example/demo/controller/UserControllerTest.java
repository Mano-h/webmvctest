package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    private static User userData ;

  @BeforeAll
  static void beforeAll() {
      userData = new User();
      userData.setName("hermann");
      userData.setId(1l);
      userData.setEmail("yemdjeu@ya.fr");
  }

  @Test
  void testGetUserById() throws Exception {

    // Mock service
    Mockito.when(userService.findUserById(any())).thenReturn(userData);

    mockMvc
        .perform(MockMvcRequestBuilders.get("/v1/user/1"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("hermann"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("yemdjeu@ya.fr"))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andDo(print());
  }


    @Test
    void testSaveUser() throws Exception {

        // Mock service
        Mockito.when(userService.save(any())).thenReturn(userData);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/v1/user/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(userData)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("hermann"))
        .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("yemdjeu@ya.fr"))
        .andExpect(status().isCreated())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        .andDo(print());
    }

}
