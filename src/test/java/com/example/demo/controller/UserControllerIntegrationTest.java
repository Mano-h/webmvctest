package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIntegrationTest {
  @Autowired private TestRestTemplate testRestTemplate;
  @Autowired private static UserService userService;

  @BeforeAll
  static void beforeAll() {}

  @Test
  @Sql("/data.sql")
  void testGetUserByid() {
    ResponseEntity<User> response = testRestTemplate.getForEntity("/v1/user/1", User.class);

    Assert.assertEquals(java.util.Optional.of(1L).get(), response.getBody().getId());
    Assert.assertEquals("h@ya.fr", response.getBody().getEmail());
    Assert.assertEquals("20/02/12", response.getBody().getBirthdate());
  }

  @Test
  void testSaveUser() {
    User user = new User();
    user.setId(1L);
    user.setName("Raoul");
    user.setBirthdate("20/02/21");
    user.setEmail("r@yahoo.fr");

    HttpEntity<User> request = new HttpEntity<>(user);

    ResponseEntity<User> response =
        testRestTemplate.postForEntity("/v1/user/", request, User.class);

    Assert.assertEquals(java.util.Optional.of(1L).get(), response.getBody().getId());
    Assert.assertEquals("r@yahoo.fr", response.getBody().getEmail());
    Assert.assertEquals("Raoul", response.getBody().getName());
    Assert.assertEquals("20/02/21", response.getBody().getBirthdate());
  }
}
